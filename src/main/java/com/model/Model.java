package com.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Model {


    public String stringUtilTest() {
        StringUtils stringUtils = new StringUtils();
        String result =
                stringUtils.getString(
                        "str1", "str2", "str3", "str4");
        return result;
    }

    public boolean  checksentence(String sentence){
        return sentence.matches("[A-Z][.[^\\.]]*\\.");
    }

    public List<String> splitSentence(String sentence){
        List<String> list = new ArrayList<>();
        String[] array = sentence.split("\\sthe\\s|\\syou\\s");
        Arrays.stream(array).forEach(s->list.add(s));
        return list;
    }

    public String replaceVowels(String sentence){
        return sentence.replaceAll("A|E|I|O|U|Y|a|e|i|o|u|y", "_");
    }

}
