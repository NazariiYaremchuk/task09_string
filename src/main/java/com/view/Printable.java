package com.view;

@FunctionalInterface
public interface Printable {
    void print();
}
