package com.view;

import com.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class ConsoleView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(ConsoleView.class);

    private Locale locale;
    private ResourceBundle resourceBundle;


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        //      menu.put("7", resourceBundle.getString("7"));
        menu.put("Q", resourceBundle.getString("Q"));
    }

    public ConsoleView() {

        controller = new Controller();
        input = new Scanner(System.in);

        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testStringUtil);
        methodsMenu.put("2", this::internationalizeMenu);
        methodsMenu.put("3", this::checkSentence);
        methodsMenu.put("4", this::splitSentence);
        methodsMenu.put("5", this::replaceVowels);

    }

    private void checkSentence() {
        logger.info("Enter sentence to check by pattern: ");
        Scanner scanner = new Scanner(System.in);
        logger.info(controller.checkSentence(scanner.nextLine()));
    }
    private void splitSentence() {
        logger.info("Enter sentence to check by pattern: ");
        Scanner scanner = new Scanner(System.in);
        logger.info((controller.splitSentence(scanner.nextLine()).toString()));
    }

    private void replaceVowels() {
        logger.info("Enter sentence to replace English vowels: ");
        Scanner scanner = new Scanner(System.in);
        logger.info((controller.replaceVowels(scanner.nextLine())));
    }



    private void testStringUtil() {
        logger.info(controller.testStringUtil());
    }

    private void internationalizeMenu() {
        logger.info("Enter localization name like \"en\" (cs, de, en, uk, it)");
        Scanner scanner = new Scanner(System.in);
        try {
            locale = new Locale(scanner.nextLine());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        resourceBundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
    }


    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
