package com.controller;

import com.model.Model;

import java.util.List;

public class Controller {
    Model model = new Model();

    public String testStringUtil() {
        return model.stringUtilTest();
    }

    public boolean checkSentence(String sentence) {
        return model.checksentence(sentence);
    }
    public List<String> splitSentence(String sentence) {
        return model.splitSentence(sentence);
    }
    public String replaceVowels(String sentence){
        return model.replaceVowels(sentence);
    }
}
