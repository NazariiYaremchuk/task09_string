package BigTask.view;

import BigTask.controller.BookController;
import com.view.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class BookConsoleView {


    private BookController controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(BookConsoleView.class);

    private Locale locale;
    private ResourceBundle resourceBundle;


    private void setMenu() {
        menu = new LinkedHashMap<>();

        //******
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));

        menu.put("I", resourceBundle.getString("I"));
        menu.put("Q", resourceBundle.getString("Q"));
        //******
    }

    public BookConsoleView() {

        controller = new BookController();
        input = new Scanner(System.in);

        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("BookMenu", locale);
        controller.getTextFromFile();
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        //*********
        methodsMenu.put("1", this::getSimilarSentencesNumber);
        methodsMenu.put("2", this::getSortedSentences);
        methodsMenu.put("3", this::getUniqueWord);
        methodsMenu.put("4", this::getWordsFromQuastions);
        methodsMenu.put("5", this::replaceBiggestWords);
        methodsMenu.put("6", this::getSortedWords);
        methodsMenu.put("I", this::internationalizeMenu);
        //*********

    }

    //task1
    private void getSimilarSentencesNumber() {
        logger.info(controller.getSimilarSentencesNumber());
    }

    //task2
    private void getSortedSentences() {
        logger.info(controller.getSortedSentenceList().toString());
    }

    //task3
    private void getUniqueWord() {
        logger.info(controller.getUniqueWord().getWord());
    }

    //task4
    private void getWordsFromQuastions() {
        logger.info("Enter length: ");
        Scanner scanner = new Scanner(System.in);
        logger.info(controller.getWordsFromQuestions(scanner.nextInt()));
    }

    private void internationalizeMenu() {
        logger.info("Enter localization name like \"en\" (cs, de, en, uk, it)");
        Scanner scanner = new Scanner(System.in);
        try {
            locale = new Locale(scanner.nextLine());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        resourceBundle = ResourceBundle.getBundle("BookMenu", locale);
        setMenu();
    }

    //task5
    private void replaceBiggestWords() {
        logger.info(controller.replaseBiggestWords());
    }

    //task6
    private void getSortedWords() {
        logger.info(controller.getSortedWords());
    }


    //-------------------------------------------------------------------------
    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }


    public static void main(String[] args) {
        new BookConsoleView().show();
    }
}
