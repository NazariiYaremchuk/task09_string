package BigTask.model;

public class Word implements SentenceElement {
    private String word;

    public Word(String word) {
        this.word = word;
    }


    public String getWord() {
        return word;
    }

    @Override
    public String toString() {
        return word;
    }

    @Override
    public int getLength() {
        return word.length();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != Word.class) return false;
        return word.equals(((Word) obj).getWord());
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }
}
