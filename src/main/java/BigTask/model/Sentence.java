package BigTask.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    private ArrayList<SentenceElement> elementsArrayList;


    Sentence(String sentence) {
        elementsArrayList = new ArrayList<>();
        elementsArrayList.addAll(parseSentence(sentence));
    }

    private ArrayList<SentenceElement> parseSentence(String sentence) {

        ArrayList<SentenceElement> sentenceElements = new ArrayList<>();

        Pattern pattern = Pattern.compile("(?<basic>\\w+)(?<punctual>[ |.|-|:|,|;|?|!])");
        Matcher matcher = pattern.matcher(sentence);

        while (matcher.find()) {
            sentenceElements.add(new Word(matcher.group("basic")));
            String tmp = matcher.group("punctual");

            if (!tmp.equals(" ") && tmp.length() == 1)
                sentenceElements.add(new PunctuationMark(tmp));
        }

        return sentenceElements;
    }


    public ArrayList<SentenceElement> getElementsArrayList() {
        return elementsArrayList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        elementsArrayList.stream().forEach(
                s -> sb.append(s.toString() + " "));

        return sb.toString();
    }

    public boolean contain(Word word) {
        return elementsArrayList.stream()
                .anyMatch(element -> word.equals(element));
    }

    public int countRepeating(Word word) {
        return (int) elementsArrayList.stream()
                .filter(element -> word.equals(element)).count();
    }

    public PunctuationMark getMainPunctuationMark() {
        SentenceElement lastElement = elementsArrayList.get(elementsArrayList.size() - 1);
        if (lastElement instanceof PunctuationMark)
            return (PunctuationMark) lastElement;
        else return new PunctuationMark(".");
    }

    @Deprecated
    public int getMaxRepeating() {
        AtomicReference<Word> maxWord = null;
        final int[] maxRepeating = new int[1];
        AtomicInteger currentRepeating = new AtomicInteger();

        elementsArrayList.stream().filter(element -> element instanceof Word)
                .forEach(word -> {
                    elementsArrayList.stream().forEach(element -> {
                        if (element.equals(word)) {
                            currentRepeating.getAndIncrement();
                        }
                        if (currentRepeating.get() > maxRepeating[0]) {
                            maxRepeating[0] = currentRepeating.get();
                            maxWord.set((Word) word);
                        }
                    });
                });
        return maxRepeating[0];
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public int getWordCount() {
        return (int) elementsArrayList.stream()
                .filter(el -> el instanceof Word).count();
    }

    private int getBiggestWordIndex() {
        int maxLength = 0;
        int index = 0;
        for (SentenceElement se : elementsArrayList) {
            int currentLength = se.getLength();
            if (currentLength > maxLength) {
                maxLength = currentLength;
                index = elementsArrayList.indexOf(se);
            }
        }
        return index;
    }

    /**
     * Replace the first word that begins to the longest letter with the longest letter
     */
    public void replaceWords() {
        String value = new String();
        int indexOfFirstElement = 0;
        int indexOfSecondElement = 0;
        indexOfSecondElement = getBiggestWordIndex();

        do {
            for (SentenceElement se : elementsArrayList)
                if (se instanceof Word) {
                    if (((Word) se).getWord().matches("^[A|E|I|O|U|Y|a|e|i|o|u|y]+.*")) {
                        indexOfFirstElement = elementsArrayList.indexOf(se);
                        value = ((Word) se).getWord();
                        break;
                    }
                }
            break;
        } while (true);

        SentenceElement tmp = elementsArrayList.get(indexOfSecondElement);
        elementsArrayList.set(indexOfSecondElement, elementsArrayList.get(indexOfFirstElement));
        elementsArrayList.set(indexOfFirstElement, tmp);
    }

}
