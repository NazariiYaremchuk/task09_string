package BigTask.model;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Book {
    private StringBuilder text;
    List<Sentence> sentenceList;

    public Book() {
        text = new StringBuilder();
        sentenceList = new LinkedList<>();
    }

    public void appendLine(String line) {
        text.append(line);
    }

    /**
     * Parse text on sentences and add them to sentenceList
     */
    public void updateList() {
        Arrays.stream(text.toString().split("(?<=[!.?])"))
                .forEach(s -> {
                    sentenceList.add(new Sentence(s));
                });
    }

    public StringBuilder getText() {
        return text;
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }

    @Override
    public String toString() {
        String info = new String("");
        for (Sentence s : sentenceList) {
            info += s.toString() + "\n";
        }
        return info;
    }

    public Map<Sentence, Boolean> containWord(Word word) {
        Map<Sentence, Boolean> containMap = new HashMap<>();
        sentenceList.stream().forEach(sentence -> {
            containMap.put(sentence, sentence.contain(word));
        });
        return containMap;
    }

    public Map<Sentence, Integer> calculateWordRepeating(Word word) {
        Map<Sentence, Integer> containMap = new HashMap<>();
        sentenceList.stream().forEach(sentence -> {
            int count = sentence.countRepeating(word);
            containMap.put(sentence, count);
        });
        return containMap;
    }

    //task1
    public int getSimilarSentencesNumber() {
        Map<Word, Integer> wordsInfo = new LinkedHashMap<>();
        Map<Sentence, Map<Word, Integer>> sentencesInfo = new LinkedHashMap<>();

        Word maxWord;
        AtomicInteger maxCount = new AtomicInteger();
        Map<Word, Integer> wi = new HashMap<>();
        for (Sentence sentence : sentenceList) {
            //for each sentence
            sentence.getElementsArrayList().stream()
                    //for each element of current sentence
                    .filter(sentenceElement ->
                            sentenceElement instanceof Word)
                    //for each word of current sentence
                    .forEach(
                            word -> sentenceList
                                    .stream().
                                            forEach(sentence1 -> {
                                                int current = sentence1.getMaxRepeating();
                                                if (maxCount.get() > current) maxCount.set(current);
                                            }));
        }
        return 0;
    }

    //task2
    public List<Sentence> sortedSentences() {
        List<Sentence> sorted = new ArrayList<>(sentenceList);
        Collections.sort(sorted, Comparator.comparingInt(Sentence::getWordCount));
        return sorted;
    }

    //task3
    public Word getUniqueWord(Sentence basicSentence) {
        Word uniqueWord = null;
        for (SentenceElement el : basicSentence.getElementsArrayList()) {
            if (el instanceof Word) {
                AtomicBoolean isUnique = new AtomicBoolean(true);

                for (Sentence sentence : sentenceList) {
                    if (!sentence.equals(basicSentence)) {
                        if (sentence.contain((Word) el)) isUnique.set(false);
                    }
                }

                if (isUnique.get()) {
                    uniqueWord = new Word(((Word) el).getWord());
                    return uniqueWord;
                }

            }
        }
        return null;
    }

    //task4
    public ArrayList<SentenceElement> getWordsFromQuestions(int length) {
        ArrayList<SentenceElement> words = new ArrayList<>();
        List<Sentence> questionSentences = getQuestionsSentences();
        for (Sentence sentence : questionSentences) {
            sentence.getElementsArrayList().stream()
                    .filter(sentenceElement -> {
                        if ((sentenceElement instanceof Word) &&
                                (sentenceElement.getLength() == length)) return true;
                        else return false;
                    }).distinct()
                    .collect(Collectors.toCollection(() -> words));
        }
        return words;
    }

    private List<Sentence> getQuestionsSentences() {
        List<Sentence> questionSentences;
        questionSentences = sentenceList.stream()
                .filter(sentence -> sentence.getMainPunctuationMark().mark == '?')
                .collect(Collectors.toList());

        return questionSentences;
    }

    //task5
    public void replaceBiggestWords() {
        List<Sentence> elements;
        sentenceList.stream().forEach(sentence -> sentence.replaceWords());

    }

    //task6
    public ArrayList<Word> getSortedWords() {
        ArrayList<Word> words = new ArrayList<>();
        for (Sentence sentence : sentenceList) {
            sentence.getElementsArrayList().stream().forEach(sentenceElement -> {
                if (sentenceElement instanceof Word) words.add((Word) sentenceElement);
            });
        }
        Collections.sort(words, (a,b)->a.getWord().toLowerCase().compareTo(b.toString().toLowerCase()));
        return words;
    }
}
