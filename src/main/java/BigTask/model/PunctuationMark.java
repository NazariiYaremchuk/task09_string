package BigTask.model;

public class PunctuationMark implements SentenceElement {
//    private static Character[] punctuals;

    //static {
//    punctuals = new Character[]{',', '.', ';', ':'};
//}
//    public static boolean isPunctual(String s) {
//        for (Character c : punctuals) {
//            if (s == String.valueOf(c)) return true;
//        }
//        return false;
//    }
    char mark;

    PunctuationMark(String s) {
        if (s.length() > 1) throw new RuntimeException("String isn`t punctual mark");
        mark = s.charAt(0);
    }


    @Override
    public String toString() {
        return String.valueOf(mark);
    }

    @Override
    public int getLength() {
        return 1;
    }
}
