package BigTask.model;

public interface SentenceElement {
    String toString();
    int getLength();
}
