package BigTask.controller;


import BigTask.model.Book;
import BigTask.model.Sentence;
import BigTask.model.Word;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BookController {
    Book book = new Book();

    public void getTextFromFile() {
        String filename = "data.txt";

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            reader.lines().forEach(s -> book.appendLine(s));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        book.updateList();
        System.out.println(book.toString());
    }

    public void contains(String word) {
        Map<Sentence, Integer> map = book.calculateWordRepeating(new Word(word));
        System.out.println(map.toString());
    }

    public int getSimilarSentencesNumber() {
        return book.getSimilarSentencesNumber();
    }
    public List<Sentence> getSortedSentenceList(){
        return book.sortedSentences();
    }
    public Word getUniqueWord(){
        return book.getUniqueWord(book.getSentenceList().get(0));
    }
    public String getWordsFromQuestions(int length){
        return book.getWordsFromQuestions(length).toString();
    }
    public String replaseBiggestWords(){
        book.replaceBiggestWords();
        return book.getSentenceList().toString();
    }
    public String getSortedWords(){
        StringBuilder sb = new StringBuilder("\n\t");
        ArrayList<Word> words = book.getSortedWords();
        for(int i=0; i<words.size()-1; i++){
            sb.append(words.get(i).getWord() + "\n");

            Pattern pattern = Pattern.compile("(?<firstSymbol>[a-zA-Z]).*");
            Matcher matcher = pattern.matcher(words.get(i).getWord());
            matcher.find();

            String current = matcher.group("firstSymbol");

            matcher = pattern.matcher(words.get(i+1).getWord());
            matcher.find();
            String next = matcher.group("firstSymbol");

            if(!current.equals(next)) sb.append("\t");
        }
        sb.append(words.size()-1);

        return sb.toString();
    }

}
